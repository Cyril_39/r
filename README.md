/* HTML */
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>weaponeGallerey</title>
  <link href="wrk.css" rel="stylesheet">
</head>
<body>
<header>
  <h1>Галерея оружия<img src="https://image.flaticon.com/icons/svg/238/238963.svg" class="logo" width="130" height="140" alt="Галерея шляп" class="badge"></h1>
  <nav>
	   <ul>
		 <li><a class="button" href="#">Главная</a></li>
		 <li><a class="button" href="#">оружие</a></li>
	     <li><a class="button" href="#">О нас</a></li>
	     <li><a class="button" href="#">Контакты</a></li>
	   </ul>
   </nav>
</header>
  <div class="gallery">
    <figure>
      <img src="images/Beretta.jpg" width="300" height="210" alt="M107A1 Barrett">
      <figcaption><strong>M107A1 Barrett</strong>Barret M107A1 — 12.7мм. Максимальная дальность стрельбы — около 1500 метров.  Эта антиматериальная винтовка — предназначенная, в первую очередь, для уничтожения не живой силы, а техники противника.</figcaption>
    </figure>
    <figure>
      <img src="images/BCC.jpg" width="300" height="210" alt="Капор">
      <figcaption><strong>ВСС "Винторез"</strong>ВСС — 9мм. Максимальная дальность стрельбы — 400 метров. Бесшумная снайперская винтовка для подразделений специального назначения.</figcaption>
    </figure>
    <figure>
      <img src="images/Mosina.jpg" width="300" height="210" alt="Розовая шляпа">
      <figcaption><strong>Мосина.</strong>Винтовка Мосина - 7.62мм. Максимальная дальность стрельбы — 2000 метров. Русская 3-линейная магазинная винтовка образца 1891 года, принятая на вооружение Русской Империи в 1891 году.</figcaption>
    </figure>
    <figure>
      <img src="images/CKAR.jpg" width="300" height="210" alt="Много шляп">
      <figcaption><strong>SCAR-L(5.56) SCAR-H(7.62)</strong>FN SCAR — 5.56мм и 7.62мм. Максимальная дальность стрельбы - 900 метров. Оружейная система, разработанная американским подразделением бельгийской компании.</figcaption>
    </figure>
    <figure>
      <img src="images/pecheneg.jpg" width="300" height="210" alt="Шляпа">
      <figcaption><strong>ПКП ""Печенег""</strong>Пулемёт «Печенег» — 7.62мм. Максимальная дальность стрельбы - 1500 метров. Российский единый пулемёт, разработанный на основе пулемёта Калашникова Модернизированного.</figcaption>
    </figure>
    <figure>
      <img src="images/MP43.jpg" width="300" height="210" alt="Другой капор">
      <figcaption><strong>Sturmgewehr 44 (МР-43)</strong>Sturmgewehr 44 - 7.92мм. Максимальная дальность стрельбы - 600 метров. В середине второй мировой войны это оружие считалось самым мощным, так как имело высокую скорострельность пп(пистолет-пулемет) и мощь винтовки.</figcaption>
    </figure>
    <figure>
      <img src="images/Drob.jpg" width="300" height="210" alt="Дурень в шляпе">
      <figcaption><strong>SRM Arms model 1216</strong>SRM 1216 - дробовик 12-го калибра с магазином на 16 патрон и стволом в 457 миллиметров. Отличается необычной конструкцией с полусвободным затвором. Оружие комплектуется подствольным барабанным магазином.</figcaption>
    </figure>
    <figure>
      <img src="images/Glock.jpg" width="300" height="210" alt="Клевая шляпа">
      <figcaption><strong>Glock 17</strong>Glock 17 - 9мм. Максимальная дальность стрельбы - 50 метров. В конструкции пистолета активно используется ударопрочный и термостойкий пластик (полиамид), что делает Glock 17 очень прочным и легким.</figcaption>
    </figure>
    <figure>
      <img src="images/MP5.jpg" width="300" height="210" alt="Крутые шляпы">
      <figcaption><strong>HK MP5</strong>Heckler & Koch MP5 — 9мм. Максимальная дальность стрельбы - 200 метров. семейство пистолетов-пулемётов, разработанных немецким производителем стрелкового оружия. Эксплортируется с 1966 года.</figcaption>
    </figure>
  </div>
  <footer>
    <div class="copyright">
      <p>Моя собсвтенность 2к19</p>
    </div>
  </footer>
</body>
</html>


/* CSS */================================================================================================================
body {
	max-width: 1024px;
	margin: 0 auto;
	background: radial-gradient(circle farthest-corner at 100px 50px, #FBF2EB, #352A3B);;
}
header h1 {
	font-size: 75px;
	padding-left: 120px;
	background: linear-gradient(45deg, #2F4F4F, #708090);
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;
	display: table;
}
nav ul{
  margin-top: -30px;
}
nav li {
	display: inline;
}
nav a {
	display: inline-block;
	padding: 9px 20px 5px 20px;
	border-radius:20px;
	text-decoration: none;
	font-weight: 700;
	color: white;
	text-shadow: 2px 2px 4px rgba(0,0,0,1);
	text-transform:uppercase;
	font-size: .9em;
	letter-spacing: .1em;
	background-color: #4c9be9;
	border: 1px solid black;
	margin-right: 5px;
}
figcaption strong {
	font-weight: 700;
	display: block;
}
footer .copyright {
	font-weight: 700;
	color: white;
	margin: 0 auto;
  margin-top: -20px;
  margin-bottom: -20px;
	max-width: 1024px;
}
header {
  position: relative;
  margin-top: 20px;
  padding: 20px 0 0 10px;
}
header .badge {
  position: absolute;
  margin-left: -679px;
  margin-top: -90px;
}
header nav {
  position: absolute;
  right: 0;
  top: 45px;
}
.gallery figure {
  display: inline-block;
  width: 300px;
  height: 210px;
  margin: 15px;
  position: relative;
}
.gallery figcaption {
  position: absolute;
  top: 15%;
  bottom: 15%;
  background-color: rgba(153,153,153,.9);
  padding: 20px;
  font-weight: 400;
  font-size: .9em;
  color: white;
  opacity: 0;
  transition: opacity .7s ease-out;
}
.gallery figure:hover figcaption {
  opacity: 1;
}
footer {
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 10px;
  background-color: black;
  color: white;
}
.button{
	background: -webkit-gradient(linear, 0 0, 0  100%, from(#f2f2f2), to(#252525), color-stop(0.6, #777), color-stop(0.6,  #404040));
	color: #fff;
	border: 2px solid  #666
}


.logo {
	position: absolute;
	z-index: 100;

	left: 0;
	top: 10px;
}

@-webkit-keyframes logo {
  from {
    -webkit-transform: rotate(0) scale(.5);
    left: 120%;
  }
  50% {
    -webkit-transform: rotate(-720deg) scale(.5);
    left: 0;
  }

  to {
    -webkit-transform: rotate(0) scale(1);
  }
}
@keyframes logo {
  from {
    transform: rotate(0) scale(.5);
    left: 120%
  }

  50% {
    transform: rotate(-720deg) scale(.5);
    left: 0;
  }

  to {
    transform: rotate(0) scale(1);
  }
}

.logo {
    -webkit-animation: logo 3s;
    animation: logo 3s;
}


